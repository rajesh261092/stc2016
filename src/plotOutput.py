import matplotlib.pyplot as plt
import numpy as np
import sys

fig, ax = plt.subplots()

outputFile = open(sys.argv[1])
lines=outputFile.readlines()
bins=[]
xValues=np.arange(120,122,0.05)
for i in xValues:
    bins.append(0)
for line in lines:
    value = (float(line.split(' ')[5]))/1000000000
    j=0
    for i in xValues:
        if(value < i):
            bins[j] = bins[j] + 1
            break
        j = j + 1

print xValues
print bins
print len(bins)
x1=np.arange(len(bins))
print x1
ax.bar(x1+0.25, bins, 0.5, facecolor='#9999ff', edgecolor='white')
for x,y in zip(x1, bins):
    ax.text(x, y, '', ha='center', va='top')
ax.set_xticks(x1+0.5)
xValues2=[]
for i in range(len(bins)):
    if(i%2):
        xValues2.append(xValues[i])
    else:
        xValues2.append('')
print xValues2
ax.set_xticklabels(xValues2)
ax.set_title("Leakage Histogram")
ax.set_xlabel('Leakage (mW)')
ax.set_ylabel('Count')
plt.xlim(0,len(bins)+1)
plt.ylim(0,250)
plt.show()
