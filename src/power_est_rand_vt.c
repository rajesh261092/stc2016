#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

typedef struct CELL_COUNT_X_
{
   char cell_name[20];
   int cell_count;

}CELL_COUNT_X;

typedef struct CELL_POWER_X_
{
   char cell_name[20];
   double static_leakage;
   double vt;

}CELL_POWER_X;

double generate_random(double max, double min)
{
   double random;
   double diff = max - min;
   random = diff/RAND_MAX * rand() + min;
   return (roundl(random * 10000) / 10000); 
}

int main (void)
{
   FILE *fd = NULL;
   double *P_LEAKAGE[241];
   int i = 0;
   int j = 0;
   double power_leakage = 0.0;
   double rand_vt;
   CELL_COUNT_X cell_count_x[241] = {};
   CELL_POWER_X cell_power_x[24341] = {};
   int flag = 0;
   double max_leakage = 0.0;


   srand(time(NULL));

   /************************************************************************************/

   fd = fopen("../output/cellCount.txt","r");

   i = 0;
   while(fscanf(fd,"%s %d",cell_count_x[i].cell_name, &cell_count_x[i].cell_count)!=EOF)
   {
      //printf("%s\n", cell_count_x[i].cell_name);
      //printf("%d\n", cell_count_x[i].cell_count);
      P_LEAKAGE[i] = calloc(cell_count_x[i].cell_count, sizeof(double));
      i++;	
   }


   /************************************************************************************/

   fd = fopen("../output/cellLeakageTable-VT.txt","r");

   i = 0;
   while(fscanf(fd,"%s %lf %lf", cell_power_x[i].cell_name, &cell_power_x[i].static_leakage, &cell_power_x[i].vt)!=EOF)
   {
      //printf("%s\n", cell_power_x[i].cell_name);
      //printf("%lf\n", cell_power_x[i].static_leakage);
      //printf("%lf\n", cell_power_x[i].vt);
      i++;
   }

   /************************************************************************************/
   // Power leakage for all cells of one type
 int iter=0;
 double error=100.0,avg_power_leakage=0.0,new_avg_power_leakage=0.0;
 //while((error>0.01)||(error<-0.01)) {
 for(iter=0;iter<2000;iter++) {
 //  iter=iter+1;
   printf("Iteration %d \t",iter);

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[0].cell_count; j++)
   {
      rand_vt = generate_random(0.4032, 0.3648);
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[0].cell_name) == 0)
	 {
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[0][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }


   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[1].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[1].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[1][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[2].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[2].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[2][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[3].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[3].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[3][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[4].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[4].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[4][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[5].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[5].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[5][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[6].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[6].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[6][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[7].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[7].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[7][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[8].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[8].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[8][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[9].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[9].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[9][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[10].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[10].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[10][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[11].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[11].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[11][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[12].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[12].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[12][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[13].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[13].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[13][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[14].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[14].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[14][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[15].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[15].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[15][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[16].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[16].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[16][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[17].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[17].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[17][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[18].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[18].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[18][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[19].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[19].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[19][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[20].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[20].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[20][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[21].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[21].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[21][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[22].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[22].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[22][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[23].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[23].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[23][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[24].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[24].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[24][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[25].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[25].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[25][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[26].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[26].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[26][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[27].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[27].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[27][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[28].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[28].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[28][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[29].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[29].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[29][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[30].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[30].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[30][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[31].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[31].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[31][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[32].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[32].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[32][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[33].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[33].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[33][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[34].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[34].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[34][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[35].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[35].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[35][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[36].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[36].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[36][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[37].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[37].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[37][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[38].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[38].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[38][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[39].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[39].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[39][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[40].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[40].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[40][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[41].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[41].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[41][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[42].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[42].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[42][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[43].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[43].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[43][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[44].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[44].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[44][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[45].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[45].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[45][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[46].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[46].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[46][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[47].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[47].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[47][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[48].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[48].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[48][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[49].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[49].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[49][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[50].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[50].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[50][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[51].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[51].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[51][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[52].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[52].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[52][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[53].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[53].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[53][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[54].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[54].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[54][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[55].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[55].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[55][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[56].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[56].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[56][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[57].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[57].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[57][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[58].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[58].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[58][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[59].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[59].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[59][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[60].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[60].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[60][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[61].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[61].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[61][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[62].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[62].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[62][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[63].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[63].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[63][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[64].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[64].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[64][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[65].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[65].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[65][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[66].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[66].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[66][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[67].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[67].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[67][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[68].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[68].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[68][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[69].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[69].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[69][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[70].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[70].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[70][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[71].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[71].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[71][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[72].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[72].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[72][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[73].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[73].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[73][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[74].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[74].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[74][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[75].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[75].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[75][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[76].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[76].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[76][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[77].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[77].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[77][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[78].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[78].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[78][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[79].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[79].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[79][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[80].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[80].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[80][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[81].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[81].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[81][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[82].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[82].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[82][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[83].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[83].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[83][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[84].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[84].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[84][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[85].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[85].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[85][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[86].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[86].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[86][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[87].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[87].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[87][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[88].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[88].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[88][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[89].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[89].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[89][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[90].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[90].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[90][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[91].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[91].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[91][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[92].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[92].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[92][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[93].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[93].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[93][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[94].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[94].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[94][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[95].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[95].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[95][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[96].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[96].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[96][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[97].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[97].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[97][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[98].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[98].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[98][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[99].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[99].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[99][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[100].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[100].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[100][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[101].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[101].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[101][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[102].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[102].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[102][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[103].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[103].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[103][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[104].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[104].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[104][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[105].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[105].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[105][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[106].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[106].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[106][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[107].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[107].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[107][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[108].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[108].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[108][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[109].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[109].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[109][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[110].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[110].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[110][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[111].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[111].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[111][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[112].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[112].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[112][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[113].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[113].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[113][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[114].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[114].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[114][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[115].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[115].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[115][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[116].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[116].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[116][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[117].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[117].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[117][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[118].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[118].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[118][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[119].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[119].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[119][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[120].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[120].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[120][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[121].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[121].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[121][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[122].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[122].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[122][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[123].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[123].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[123][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[124].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[124].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[124][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[125].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[125].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[125][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[126].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[126].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[126][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[127].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[127].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[127][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[128].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[128].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[128][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[129].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[129].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[129][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[130].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[130].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[130][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[131].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[131].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[131][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[132].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[132].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[132][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[133].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[133].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[133][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[134].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[134].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[134][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[135].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[135].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[135][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[136].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[136].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[136][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[137].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[137].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[137][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[138].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[138].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[138][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[139].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[139].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[139][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[140].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[140].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[140][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[141].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[141].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[141][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[142].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[142].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[142][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[143].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[143].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[143][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[144].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[144].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[144][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[145].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[145].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[145][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[146].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[146].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[146][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[147].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[147].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[147][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[148].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[148].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[148][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[149].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[149].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[149][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[150].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[150].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[150][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[151].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[151].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[151][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[152].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[152].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[152][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[153].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[153].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[153][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[154].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[154].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[154][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[155].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[155].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[155][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[156].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[156].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[156][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[157].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[157].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[157][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[158].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[158].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[158][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[159].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[159].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[159][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[160].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[160].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[160][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[161].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[161].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[161][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[162].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[162].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[162][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[163].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[163].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[163][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[164].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[164].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[164][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[165].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[165].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[165][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[166].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[166].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[166][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[167].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[167].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[167][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[168].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[168].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[168][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[169].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[169].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[169][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[170].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[170].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[170][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[171].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[171].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[171][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[172].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[172].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[172][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[173].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[173].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[173][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[174].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[174].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[174][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[175].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[175].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[175][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[176].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[176].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[176][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[177].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[177].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[177][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[178].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[178].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[178][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[179].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[179].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[179][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[180].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[180].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[180][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[181].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[181].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[181][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[182].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[182].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[182][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[183].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[183].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[183][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[184].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[184].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[184][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[185].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[185].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[185][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[186].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[186].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[186][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[187].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[187].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[187][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[188].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[188].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[188][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[189].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[189].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[189][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[190].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[190].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[190][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[191].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[191].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[191][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[192].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[192].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[192][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[193].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[193].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[193][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[194].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[194].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[194][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[195].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[195].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[195][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[196].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[196].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[196][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[197].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[197].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[197][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[198].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[198].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[198][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[199].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[199].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[199][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[200].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[200].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[200][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[201].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[201].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[201][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[202].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[202].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[202][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[203].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[203].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[203][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[204].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[204].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[204][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[205].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[205].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[205][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[206].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[206].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[206][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[207].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[207].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[207][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[208].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[208].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[208][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[209].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[209].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[209][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[210].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[210].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[210][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[211].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[211].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[211][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[212].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[212].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[212][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[213].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[213].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[213][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[214].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[214].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[214][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[215].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[215].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[215][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[216].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[216].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[216][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[217].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[217].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[217][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[218].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[218].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[218][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[219].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[219].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[219][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[220].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[220].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[220][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[221].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[221].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[221][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[222].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[222].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[222][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[223].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[223].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[223][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[224].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[224].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[224][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[225].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[225].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[225][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[226].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[226].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[226][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[227].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[227].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[227][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[228].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[228].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[228][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[229].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[229].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[229][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[230].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[230].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[230][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[231].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[231].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[231][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[232].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[232].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[232][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[233].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[233].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[233][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[234].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[234].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[234][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[235].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[235].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[235][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[236].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[236].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[236][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[237].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[237].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.6300, 0.5700);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[237][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[238].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[238].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[238][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[239].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[239].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4914, 0.4446);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[239][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   flag = 0;
   max_leakage = 0.0;
   for(j = 0; j < cell_count_x[240].cell_count; j++)
   {
      for(i = 0; i < 24341; i++)
      {
	 if(strcmp(cell_power_x[i].cell_name, cell_count_x[240].cell_name) == 0)
	 {
	    rand_vt = generate_random(0.4032, 0.3648);
	    if(rand_vt == cell_power_x[i].vt)
	    {
	       P_LEAKAGE[240][j] =  cell_power_x[i].static_leakage;
	       flag = 1;
	       break;
	    }
	    max_leakage = cell_power_x[i].static_leakage;
	 }
      }
      if(flag == 0)
      {
	 P_LEAKAGE[0][j] = max_leakage;
      }
   }

   /************************************************************************************/

   power_leakage=0.0;
   /* total leakage = Sum of all Pleakage of all matrices */
   for(i = 0; i < 241; i++)
   {
      for(j = 0; j < cell_count_x[i].cell_count; j++)
      {
	 power_leakage += P_LEAKAGE[i][j];
      }
   }

   //printf("Power leakage = %lf and average leakage = %1f\n", power_leakage,avg_power_leakage);
   
   //new_avg_power_leakage = (((iter-1)*avg_power_leakage) + power_leakage) / iter;
   //error = 100*(new_avg_power_leakage - avg_power_leakage)/avg_power_leakage;
   //avg_power_leakage = new_avg_power_leakage;
   //printf("Power leakage = %lf and average leakage = %1f\n", power_leakage,avg_power_leakage);
   printf("Power leakage = %lf\n", power_leakage);
 }
   return 0;
}
