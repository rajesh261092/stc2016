#!/usr/bin/perl
##----------------------------------------------------------------------------------
## Author      : asayal
##----------------------------------------------------------------------------------

use strict;
use Getopt::Long;
use File::Basename;
use Cwd;
use Cwd qw(abs_path);
use POSIX qw(ceil); 
use POSIX qw(floor); 
use List::Util qw(min max);

my $help = 0;
my ($netlist,$libdir,$corner,$cell,$cell_types,$mode,$threshold_origvol,$corner_temp,$string,$outputdir,$cellReport,$processleakageReport,$templeakageReport);
my %CELL_COUNT = ();
my %CELL_LEAKAGE = ();
my $cell_count = 0;
my @all_libs = ();

my $hvt_vth = 0.6;
my $rvt_vth = 0.78*$hvt_vth;
my $lvt_vth = 0.64*$hvt_vth;


# Specify percentage variation needed
my $range = 5;

# Specify number of variations needed
my $steps = 100;

GetOptions(
    "netlist=s"       => \$netlist,
    "libdir=s"	      => \$libdir,
    "outputdir=s"     => \$outputdir,
    "corner=s"	      => \$corner,
    "mode=s"          => \$mode,	
    "help|h"          => \$help,
);


#################### MAIN ################


## Checking inputs
if (! $help) { print "INFO: Checking inputs\n";}
&checkInputs();

## Parsing netlist to generate cell count stats
print "INFO: Parsing Netlist $netlist\n";
&parseNetlist();

## Generating cell reports
print "INFO: Generating cells report $cellReport\n";
&generateCellReport();

## Get leakage power from library files
print "INFO: Getting leakage power from library\n";
&getLeakagePower();

if ($mode eq "process")
{
	print "INFO: Generating process leakage report $processleakageReport\n";
	&generateProcessLeakageReport();
}
elsif ($mode eq "temp")
{
	print "INFO: Generating temp leakage report $templeakageReport\n";
	&generateTempLeakageReport();
}
################ END OF MAIN ###############


sub checkInputs()
{

	if ($help) {&printUsage(); exit 1;}
	if (($netlist =~ /^\s*$/) or ($libdir =~ /^\s*$/) or ($corner =~ /^\s*$/) or ($mode =~ /^\s*$/) or ($outputdir =~ /^\s*$/)) {print "Error: Insufficient input arguments\n"; &printUsage(); exit 1;}
	if (! -e $netlist) {print "Error: Netlist file specified $netlist does not exist\n"; exit 1;}
	if (! -d $libdir) {print "Error: Lib Directory specified $libdir does not exist\n"; exit 1;}
	if (($mode ne "process") and ($mode ne "temp")) {print "Error: Incorrect mode name. Valid names are process and temp\n"; exit 1;}

	if (! -d $outputdir) {`mkdir $outputdir`;}

	$cellReport = "$outputdir/cellCount.txt";
	$processleakageReport = "$outputdir/cellLeakageTable-VT.txt";
	$templeakageReport = "$outputdir/cellLeakageTable-VTandT.txt";


}

sub parseNetlist()
{
	open(NET,$netlist) || die "Error: Cannot open netlist file $netlist to read\n";
	while (my $line = <NET>)
	{
		if ($line =~ /^\s*(\S+)\s+U\S+\s*\(/)
		{
			my $cell = $1;
			if ($cell !~ /module/)
			{
				if (! exists $CELL_COUNT{$cell})
				{
					$CELL_COUNT{$cell} = 1;
					$cell_count++;
				}
				else
				{
					$CELL_COUNT{$cell}++;
					$cell_count++;
				}	
			}
		}
	}
	close(NET);
}

sub generateCellReport()
{

	open(CELLREPORT,">$cellReport") || die "Error: Cannot open cells report file $cellReport to write\n";
	foreach my $cell (keys %CELL_COUNT)
	{
		print CELLREPORT "$cell,$CELL_COUNT{$cell}\n";
	}
	close(CELLREPORT);

	$cell_types = scalar(keys %CELL_COUNT);
	print "INFO: Total number of cells in design: $cell_count. Total type of cells: $cell_types\n";
}

sub getLeakagePower()
{
	my @lvt_libs = glob "$libdir/lib/stdcell_lvt/db_nldm/*$corner*lib";
	my @rvt_libs = glob "$libdir/lib/stdcell_rvt/db_nldm/*$corner*lib";
	my @hvt_libs = glob "$libdir/lib/stdcell_hvt/db_nldm/*$corner*lib";

	push(@all_libs,@lvt_libs);
	push(@all_libs,@rvt_libs);
	push(@all_libs,@hvt_libs);
	
	foreach my $lib(@all_libs)
	{
		if (-e $lib)
		{
			open(LIB,$lib) || die "Error: Cannot open lib file $lib to read\n";
			while (my $line = <LIB>)
			{
				if ($line =~ /^\s*cell\s*\(\s*(\S+)\s*\)\s*{\s*$/) 
				{
					$cell = $1;
					if ($cell =~ /"/) {$cell =~ s/"//g;}
				}
				if ($line =~ /^\s*cell_leakage_power\s*:\s*(\S+)\s*;\s*$/) 
				{
					my $leakage = $1;
					if (exists $CELL_COUNT{$cell})
					{
						$CELL_LEAKAGE{$cell} = $leakage;
						delete $CELL_COUNT{$cell};
					}
				}

			}
			close(LIB);
		}
	}

	my @leakage_not_found_cells = (keys %CELL_COUNT);
	my $cell_leakage_count_notfound = scalar(@leakage_not_found_cells);

	if (@leakage_not_found_cells)
	{
		print "Error: Leakage info not found for $cell_leakage_count_notfound cells: @leakage_not_found_cells\n";
		exit 1;
	}
}

sub generateProcessLeakageReport()
{
	open(REP,">$processleakageReport") || die "Error: Cannot open process leakage report $processleakageReport to write\n";
	
	foreach my $cell(keys %CELL_LEAKAGE)
	{
		my $leakage_origval = $CELL_LEAKAGE{$cell};
		if ($corner =~ /v(\S+)c/) {$string = $1;}
		if ($string !~ /n/) {$corner_temp = 273.15 + $string;} else {$corner_temp = 273.15 - $string;}
		
		if ($cell =~ /hvt/i) {$threshold_origvol = $hvt_vth;}
		elsif ($cell =~ /rvt/i) {$threshold_origvol = $rvt_vth;}
		elsif ($cell =~ /lvt/i) {$threshold_origvol = $lvt_vth;}
		
		for(my $i=(-$steps/2); $i<=($steps)/2;$i++)
		{
			my $delta = 2*$i*$range/(100*$steps);
			my $leakage_val = sprintf "%.3f",$leakage_origval*exp((2775/$corner_temp)*$delta);
			my $threshold_vol = sprintf "%.4f",$threshold_origvol*(1+$delta);
			print REP "$cell,$leakage_val,$threshold_vol\n";
		}
		
	}

	close(REP);
}


sub generateTempLeakageReport()
{
	open(REP,">$templeakageReport") || die "Error: Cannot open process leakage report $templeakageReport to write\n";

	if ($corner =~ /v(\S+)c/) {$string = $1;}
	if ($string !~ /n/) {$corner_temp = 273.15 + $string;} else {$corner_temp = 273.15 - $string;}
	foreach my $cell(keys %CELL_LEAKAGE)
	{
		my $leakage_origval = $CELL_LEAKAGE{$cell};
		if ($cell =~ /hvt/i) {$threshold_origvol = $hvt_vth;}
		elsif ($cell =~ /rvt/i) {$threshold_origvol = $rvt_vth;}
		elsif ($cell =~ /lvt/i) {$threshold_origvol = $lvt_vth;}
		
		for(my $i=(-$steps/2); $i<=($steps)/2;$i++)
		{
			my $delta = 2*$i*$range/(100*$steps);
			my $leakage_val = sprintf "%.3f",$leakage_origval*((1+$delta)**1.3)*exp((2775/$corner_temp)*(1-(1/sqrt(1+$delta))));
			my $threshold_vol = sprintf "%.4f",$threshold_origvol*sqrt(1+$delta);
			my $temp = $corner_temp*(1+$delta);
			print REP "$cell,$leakage_val,$threshold_vol,$temp\n";
		}
	}

	close(REP);


}


sub printUsage()
{
  print "
  $0   
  -netlist        <.v file>          (REQUIRED)
  -libdir 	  <lib files dir>    (REQUIRED)
  -outputdir      <Output directory> (REQUIRED)
  -corner	  <corner name)      (REQUIRED)
  -mode 	  <temp|process>     (REQUIRED)
  ";
}



