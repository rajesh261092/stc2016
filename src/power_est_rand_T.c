#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

typedef struct CELL_COUNT_X_
{
   char cell_name[20];
   int cell_count;

}CELL_COUNT_X;

typedef struct CELL_POWER_X_
{
   char cell_name[20];
   double static_leakage;
   double vt;
   double T;

}CELL_POWER_X;

double generate_random(double max, double min)
{
   double random;
   double diff = max - min;
   random = diff/RAND_MAX * rand() + min;
   return (roundl(random * 100000) / 100000); 
}

int main (void)
{
   FILE *fd = NULL;
   double *P_LEAKAGE[241];
   int i = 0;
   int j = 0;
   double power_leakage = 0.0;
   double rand_T;
   CELL_COUNT_X cell_count_x[241] = {};
   CELL_POWER_X cell_power_x[24341] = {};
   int flag = 0;
   double max_leakage = 0.0;
   int k = 0;


   srand(time(NULL));

   /************************************************************************************/

   fd = fopen("../output/cellCount.txt","r");

   i = 0;
   while(fscanf(fd,"%s %d",cell_count_x[i].cell_name, &cell_count_x[i].cell_count)!=EOF)
   {
      //printf("%s\n", cell_count_x[i].cell_name);
      //printf("%d\n", cell_count_x[i].cell_count);
      P_LEAKAGE[i] = calloc(cell_count_x[i].cell_count, sizeof(double));
      i++;	
   }


   /************************************************************************************/

   fd = fopen("../output/cellLeakageTable-VTandT.txt","r");

   i = 0;
   while(fscanf(fd,"%s %lf %lf %lf", 
         cell_power_x[i].cell_name, 
         &cell_power_x[i].static_leakage, 
         &cell_power_x[i].vt, 
         &cell_power_x[i].T)!=EOF)
   {
      //printf("%s\n", cell_power_x[i].cell_name);
      //printf("%lf\n", cell_power_x[i].static_leakage);
      //printf("%lf\n", cell_power_x[i].vt);
      i++;
   }

   /************************************************************************************/
   // Power leakage for all cells of one type
  int iter = 0;

  double error=100.0,avg_power_leakage = 0.0,new_avg_power_leakage = 0.0;

  //while(error > 1) {
  for(iter=0;iter<2000;iter++) 
  {  
     //iter=iter+1;
     printf("Iteration %d \t",iter);


     for(k = 0; k < 241; k++)
     {
	flag = 0;
	max_leakage = 0.0;
	for(j = 0; j < cell_count_x[k].cell_count; j++)
	{
	   rand_T = generate_random(313.0575, 283.2425);
	   for(i = 0; i < 24341; i++)
	   {
	      if(strcmp(cell_power_x[i].cell_name, cell_count_x[k].cell_name) == 0)
	      {
		 if(fabs(rand_T - cell_power_x[i].T) < 0.15)
		 {
		    P_LEAKAGE[k][j] =  cell_power_x[i].static_leakage;
		    flag = 1;
		    break;
		 }
		 max_leakage = cell_power_x[i].static_leakage;
	      }
	   }
	   if(flag == 0)
	   {
	      P_LEAKAGE[k][j] = max_leakage;
	   }
	}

     }

     power_leakage=0.0;
     i = 0;
     /* total leakage = Sum of all Pleakage of all matrices */
     for(i = 0; i < 241; i++)
     {
	for(j = 0; j < cell_count_x[i].cell_count; j++)
	{
	   power_leakage += P_LEAKAGE[i][j];
	}
     }
  
     printf("Power leakage = %lf\n", power_leakage);

  }

  return 0;
}
